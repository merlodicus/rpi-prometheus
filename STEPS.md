# Steps taken to get this set up

## On Kubernetes

### Pre-requisites

* K8 cluster prepared with Tiller

### Kubernetes setup

1. `helm install stable/prometheus` is [failing due to this issue](https://github.com/helm/charts/issues/15742), so do this instead: `docker run -p 9090:9090 -v prometheus.yml:/etc/prometheus/prometheus.yml prom/prometheus`

## On Docker (using docker-compose)

### Pre-requisites

* Docker installed
* Docker-compose installed

### Docker setup

```sh
docker-compose up -d
```
