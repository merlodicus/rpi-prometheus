from blockchain import statistics
from prometheus_client import start_http_server, Gauge, Summary
import time
import os

PROMETHEUS_CLIENT_PORT=int(os.environ['PROMETHEUS_CLIENT_PORT'])
SAMPLE_INTERVAL=15000
UPDATE_TIME = Summary('blockchain_statistics_update_time', 'Time spent updating blockchain statistics')

def create_gauges_for(
  stats
):
  statFields = [stat for stat in dir(stats) if not stat.startswith('__')]
  gauges = dict()
  for field in statFields:
    g = Gauge(field, field)
    g.set(getattr(stats, field))
    gauges[field] = g

  return gauges


@UPDATE_TIME.time()
def update_gauges(
  gauges
):
  stats = statistics.get()
  statFields = [stat for stat in dir(stats) if not stat.startswith('__')]
  for field in statFields:
    gauges[field].set(getattr(stats, field))

if __name__ == '__main__':
  stats = statistics.get()
  gauges = create_gauges_for(stats)

  print("Exposing prometheus metrics on port %s" % (PROMETHEUS_CLIENT_PORT,))
  start_http_server(PROMETHEUS_CLIENT_PORT)
  while True:
    time.sleep(SAMPLE_INTERVAL)
    update_gauges(gauges)
